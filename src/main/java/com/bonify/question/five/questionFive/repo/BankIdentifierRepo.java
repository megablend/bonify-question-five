/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.repo;

import com.bonify.question.five.questionFive.dto.BankIdentifierByIdentifier;
import com.bonify.question.five.questionFive.models.BankIdentifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Nexus Axis
 */
public interface BankIdentifierRepo extends JpaRepository<BankIdentifier, Long> {
    @Query("SELECT new com.bonify.question.five.questionFive.dto.BankIdentifierByIdentifier(b.name) FROM BankIdentifier b WHERE b.identifier = ?1")
    BankIdentifierByIdentifier findByIdentifier(String identifier);
}
