/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.utils;

import java.util.regex.Pattern;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author Nexus Axis
 */
public class Util {
    
    private final static Pattern NAME_PATTERN = Pattern.compile("(name.*)",Pattern.CASE_INSENSITIVE);
    private final static Pattern BANK_IDENTIFIER_PATTERN = Pattern.compile("(bank_identifier.*)",Pattern.CASE_INSENSITIVE);
    
    public static boolean isHeaderRow(CSVRecord record) {
        return NAME_PATTERN.matcher(record.get(0)).matches() || BANK_IDENTIFIER_PATTERN.matcher(record.get(1)).matches();
    }
}
