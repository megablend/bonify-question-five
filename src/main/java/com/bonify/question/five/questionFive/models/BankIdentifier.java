/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Nexus Axis
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "bank_identifiers", uniqueConstraints=
        @UniqueConstraint(columnNames={"name", "identifier"}))
@ToString
public class BankIdentifier implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Transient
    private Timestamp now = new Timestamp(System.currentTimeMillis());
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Column(name ="name", nullable = false, length = 50, unique = true)
    private String name;
    
    @Column(name ="identifier", length = 50, nullable = false)
    private String identifier;
    
    @Column(name = "created_at")
    @Temporal(TemporalType.DATE)
    private Date createdAt = now;
    
    @Column(name = "updated_at")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    
    @PrePersist
    public void onPrePersist() {
        updatedAt = now;
    }
}
