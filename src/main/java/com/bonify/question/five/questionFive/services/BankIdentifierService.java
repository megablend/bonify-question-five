/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.services;

import com.bonify.question.five.questionFive.dto.BankIdentifierByIdentifier;
import com.bonify.question.five.questionFive.models.BankIdentifier;
import com.bonify.question.five.questionFive.repo.BankIdentifierRepo;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nexus Axis
 */
@Service
@Slf4j
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(rollbackFor = {Exception.class})
public class BankIdentifierService {
    @Autowired
    private BankIdentifierRepo bankIdentifierRepo;
    
    public void saveAll(List<BankIdentifier> bankIdentifiers) {
        try {
            bankIdentifierRepo.saveAll(bankIdentifiers);
        } catch (Exception e) {
            log.error("Something went wrong while trying to save records", e);
        }
    }
    public BankIdentifier save(BankIdentifier bankIdentifier) {
        try {
            return bankIdentifierRepo.saveAndFlush(bankIdentifier);
        } catch (Exception e) {
            log.error("Unable to save bank identifier", e);
        }
        return null;
    }
    public BankIdentifierByIdentifier findByIdentifier(String identifier) {
        try {
            return bankIdentifierRepo.findByIdentifier(identifier);
        } catch (Exception e) {
            log.error("Unable to find bank with the identifier {}", identifier, e);
        }
        return null;
    }
}
