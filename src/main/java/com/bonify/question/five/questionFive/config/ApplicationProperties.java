/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author Nexus Axis
 */
@ConfigurationProperties(prefix = "bonify.questions.app")
@Data
@Validated
public class ApplicationProperties {
    
}
