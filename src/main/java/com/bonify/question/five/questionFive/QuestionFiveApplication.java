package com.bonify.question.five.questionFive;

import com.bonify.question.five.questionFive.dto.BankIdentifierByIdentifier;
import com.bonify.question.five.questionFive.models.BankIdentifier;
import com.bonify.question.five.questionFive.services.BankIdentifierService;
import com.bonify.question.five.questionFive.utils.Util;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class QuestionFiveApplication {
    
        @Autowired
        private BankIdentifierService bankIdentifierService;
        
        private static final char DELIMITER = ';';
        
	public static void main(String[] args) {
		SpringApplication.run(QuestionFiveApplication.class, args);
	}
        
        @Bean 
        public CommandLineRunner processFile() {
            return (args) -> {
                if (args.length != 1)
                    System.out.println("Invalid argument provided");
                else {
                    Path path = Paths.get(args[0]); // the path for the csv file :) You have to be sure
                
                    // ok --> moment of truth, time to check if the file exists
                    if (!Files.exists(path))
                        System.out.println("Oops! Seems like the path you provided does not exist:" + path.toString());

                    /* I would have used csvParser.getRecords but is so dangerous when  you have a large dataset in the file. 
                       Everything will be loaded into memory. Your system should better be SUPER! to handle that
                    */
                    try (Reader reader = Files.newBufferedReader(path); CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withDelimiter(DELIMITER))) {
                        Iterator<CSVRecord> records = csvParser.iterator();
                        List<BankIdentifier> bankIdentifiers = new ArrayList<>();
                        while(records.hasNext()) {
                            CSVRecord record = records.next();
                            if (Util.isHeaderRow(record))
                                continue;
                            BankIdentifier bankIdentifier = new BankIdentifier();
                            bankIdentifier.setName(record.get(0));
                            bankIdentifier.setIdentifier(record.get(1));
                            bankIdentifiers.add(bankIdentifier);
                        }
                        // save all the records 
                        bankIdentifierService.saveAll(bankIdentifiers);
                    } catch(Exception e) {
                        log.error("Unable to read file {}", path, e);
                    }

                    // Fetch the details of the bank with identifier 10040000
                    BankIdentifierByIdentifier bankIdentifierByName = bankIdentifierService.findByIdentifier("10040000");
                    if (null == bankIdentifierByName)
                        log.error("The bank with the identifier 10040000 does not exist");
                    else 
                        System.out.println("The name of the bank with the identifier 10040000 is " + bankIdentifierByName.getName());
                }
            };
        }
}
