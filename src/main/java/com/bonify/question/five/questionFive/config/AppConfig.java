/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Nexus Axis
 */
@Configuration
@EnableConfigurationProperties(ApplicationProperties.class)
@EnableTransactionManagement
public class AppConfig {
    
}
