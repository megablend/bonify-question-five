/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.test.services;

import com.bonify.question.five.questionFive.dto.BankIdentifierByIdentifier;
import com.bonify.question.five.questionFive.repo.BankIdentifierRepo;
import com.bonify.question.five.questionFive.services.BankIdentifierService;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Nexus Axis
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BankIdentifierServiceTest {
    @Autowired
    private BankIdentifierService bankIdentifierService;
    @MockBean
    private BankIdentifierRepo bankIdentifierRepo;
    
    @Before
    public void setup() {
        BankIdentifierByIdentifier bankIdentifier = new BankIdentifierByIdentifier("Commerzbank");
        when(bankIdentifierRepo.findByIdentifier("10010010")).thenReturn(bankIdentifier);
        when(bankIdentifierRepo.findByIdentifier("zzz")).thenReturn(null);
    }
    
    @Test
    public void testWhenValidIdentifier_thenReturnAnObject() throws Exception {
        BankIdentifierByIdentifier bankIdentifier = bankIdentifierService.findByIdentifier("10010010");
        assertNotNull(bankIdentifier);
        assertThat(bankIdentifier.getName()).isEqualTo("Commerzbank");
    }
    
    @Test
    public void testWhenInvalidIdentifier_thenReturnNull() throws Exception {
        BankIdentifierByIdentifier bankIdentifier = bankIdentifierService.findByIdentifier("zzz");
        assertNull(bankIdentifier);
    }
}
