/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.five.questionFive.test.repo;

import com.bonify.question.five.questionFive.dto.BankIdentifierByIdentifier;
import com.bonify.question.five.questionFive.models.BankIdentifier;
import com.bonify.question.five.questionFive.repo.BankIdentifierRepo;
import javax.persistence.PersistenceException;
import static junit.framework.Assert.assertNotNull;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Nexus Axis
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan("com.bonify.question.five.questionFive")
public class BankIdentifierRepoTest {
    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private BankIdentifierRepo bankIdentifierRepo;
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    /**
     * Validate the saving of records
     */
    @Test
    public void whenValidRecordProvided_saveRecords() throws Exception {
        BankIdentifier bankIdentifier = new BankIdentifier();
        bankIdentifier.setName("chioma");
        bankIdentifier.setIdentifier("ddfdffdf");
        BankIdentifier savedBankIdentifier = testEntityManager.persist(bankIdentifier);
        testEntityManager.flush();
        assertThat(bankIdentifier.getName()).isEqualTo(savedBankIdentifier.getName());
        assertNotNull(savedBankIdentifier.getId());
    }
    
    /**
     * Test Constraint Violation Exception
     */
    @Test(expected = PersistenceException.class)
    public void throwDataIntegrityException_whenDuplicateRecords() {
        BankIdentifier bankIdentifier = new BankIdentifier();
        bankIdentifier.setName("Postbank");
        bankIdentifier.setIdentifier("10010010");
        testEntityManager.persist(bankIdentifier);
        testEntityManager.flush();
        
        BankIdentifier duplicateBankIdentifier = new BankIdentifier();
        duplicateBankIdentifier.setName("Postbank");
        duplicateBankIdentifier.setIdentifier("10010010");
        testEntityManager.persist(duplicateBankIdentifier);
        testEntityManager.flush();
    }
    
    /**
     * Test Find Identifier
     * 
     */
    @Test
    public void whenFindIdentifier_thenReturnObject() throws Exception {
        BankIdentifier bankIdentifier = new BankIdentifier();
        bankIdentifier.setName("Commerzbank");
        bankIdentifier.setIdentifier("10040000");
        testEntityManager.persist(bankIdentifier);
        testEntityManager.flush();
        
        BankIdentifierByIdentifier findIdentifier = bankIdentifierRepo.findByIdentifier("10040000");
        assertThat(findIdentifier.getName()).isEqualTo("Commerzbank");
    }
    
}
